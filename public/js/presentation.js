var captionLength = 0;
var caption = '';


jQuery(document).ready(function() {
    setInterval ('cursorAnimation()', 600);
    captionEl = $('#caption');
});

function cursorAnimation() {
    $('#cursor').animate({
        opacity: 0
    }, 'fast', 'swing').animate({
        opacity: 1
    }, 'fast', 'swing');
}